#include "plane_seg/dataIo.h"
#include "plane_seg/VoxelFilter.h"
#include "plane_seg/featureCalculation.h"
#include "plane_seg/planeSegmentation.h"
#include "plane_seg/planeFeatureCalculation.h"
#include <iostream>
#include <iomanip>
#include <ros/ros.h>
#include <omp.h>

using namespace  std;


int main(int argc, char**argv)
{
        ros::init(argc, argv,"planeseg_node");
	ros::NodeHandle("~");
	
	//Input Point Cloud;
	dataIo io;
	pointCloudXYZ pointCloud;
	string input_pcd;
	ros::param::get("~input_pcd",input_pcd);
	cout<<input_pcd<<endl;
	io.readPointCloudFromPcdFileA(input_pcd, pointCloud);
	cout<< "readPointCloudFromPcdFileA........."<<endl;
	
	/*2017.1.4 remove the invalid coordinate values;*/
	for (size_t i = 0; i < pointCloud.points.size(); ++i)
	{
		int xx = isnan( pointCloud.points[i].x);
		int yy = isnan(pointCloud.points[i].y);
		int zz = isnan(pointCloud.points[i].z);

		if (xx||yy||zz)
		{
			pointCloud.points[i].x = 0.0;
			pointCloud.points[i].y = 0.0;
			pointCloud.points[i].z = 0.0;
		}
		
	}
	
	float downsample_resolution;
	ros::param::get("~downsample_resolution",downsample_resolution);
	VoxelFilter fil(downsample_resolution);
	pointCloudXYZ simplifiedPointCloud;
	fil.filter(pointCloud,simplifiedPointCloud);
	cout<< "downsamplePointCloud........"<<endl;
	
	featureCalculation fc;
	float radiusFea; 
	ros::param::get("~radiusFea",radiusFea);
	vector<featureCalculation::eigenFeature> ptFeatures (simplifiedPointCloud.points.size());
	fc.calculateEigenFeaturesBasedOnRadiusSearch(simplifiedPointCloud, radiusFea, ptFeatures);
	fc.outputFeature("feature.txt",ptFeatures);
	cout<< "calculateEigenFeatures........"<<endl;
	
	//Plane Segmentation;
	float includedAngle;
	float Pt2PlaneDis;
	float searchRadius;
	int mimPtNum;
	ros::param::get("~includedAngle",includedAngle);
	ros::param::get("~Pt2PlaneDis",Pt2PlaneDis);
	ros::param::get("~searchRadius",searchRadius);
	ros::param::get("~mimPtNum",mimPtNum);
	includedAngle=includedAngle*M_PI/180.0;
	
	string plane_seg_pcd;
	ros::param::get("~plane_seg_pcd",plane_seg_pcd);
	planeSegmentation ps;
	vector <pcl::PointIndices> planesIndices;
	ps.planeSegmentationBasedOnRegionGrowingRadius(simplifiedPointCloud, ptFeatures, includedAngle, Pt2PlaneDis, searchRadius, mimPtNum, planesIndices);
	ps.outputPlanes(plane_seg_pcd, simplifiedPointCloud, planesIndices);
	cout<< "planeSegmentationBasedOnRegionGrowingRadius........."<<endl;
	
	//plane feature calculation;
	vector<planeFeaCal::planeFeature> planesFea;
	planeFeaCal pfc;
	string plane_fea_txt;
	float mimScale;
	float angleConstrain;
	ros::param::get("~mimScale",mimScale);
	ros::param::get("~angleConstrain",angleConstrain);
	angleConstrain=angleConstrain*M_PI/180.0;
	ros::param::get("~plane_fea_txt",plane_fea_txt);
	
	pfc.calculateTheFeaturesOfPlanes(simplifiedPointCloud,planesIndices, mimScale,angleConstrain,planesFea);
	pfc.outputPlanesFea(plane_fea_txt,planesFea);
	pfc.displayPlanesAndBounds(simplifiedPointCloud, planesFea);
	cout<< "calculateTheFeaturesOfPlanes.........."<<endl;
	
	
	pointCloudXYZ().swap(pointCloud);
	pointCloudXYZ().swap(simplifiedPointCloud);
	vector<featureCalculation::eigenFeature>().swap(ptFeatures); 
	vector <pcl::PointIndices>().swap(planesIndices);
	vector <planeFeaCal::planeFeature> ().swap(planesFea);
	cout<< "DONE..."<<endl;
	
	ros::spinOnce();
        return 0;
}
