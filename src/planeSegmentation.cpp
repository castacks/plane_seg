 #include "plane_seg/planeSegmentation.h"

#include "plane_seg/featureCalculation.h"
#include "plane_seg/dataIo.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <set>
#include <deque>
#include <vector>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>

using namespace  std;

bool CmpPointCurvatureS(featureCalculation::eigenFeature a, featureCalculation::eigenFeature b)
{
	if (a.curvature<b.curvature)
	{
		return true;
	}
	else
	{
		return false;
	}
}



size_t planeSegmentation::
planeSegmentationBasedOnRegionGrowingRadius(pointCloudXYZ &pointCloud, vector<featureCalculation::eigenFeature> &ptFeatures,
	                                    float includedAngle, float Pt2PlaneDis, float searchRadius,int minPtNum,
	                                    vector <pcl::PointIndices> &planes)
	
{
	sort(ptFeatures.begin(), ptFeatures.end(), CmpPointCurvatureS);

	set< size_t, less<size_t> >unSegment;
	set< size_t, less<size_t> >::iterator iterUnseg;
	pointCloudXYZ inputCloud;
	vector<bool> hasSeg;
	for (size_t i = 0; i < ptFeatures.size(); ++i)
	{
		unSegment.insert(i);
		inputCloud.push_back(ptFeatures[i].pt);
		hasSeg.push_back(false);
	}

	

	pcl::KdTreeFLANN<pcl::PointXYZ>kdtree;
	std::vector<int>pointIdxSearch;
	std::vector<float>pointDistance;
	kdtree.setInputCloud(inputCloud.makeShared());


	

	deque <size_t>seed;
	while (!unSegment.empty())
	{
		pcl::PointIndices plane;

		iterUnseg = unSegment.begin();
		size_t minNum = *iterUnseg;

		unSegment.erase(minNum);
		seed.push_back(minNum);
		plane.indices.push_back(ptFeatures[minNum].ptIndex);
		hasSeg[minNum] = true;

		double nx, ny, nz;
		nx = ptFeatures[minNum].normalDirection.x();
		ny = ptFeatures[minNum].normalDirection.y();
		nz = ptFeatures[minNum].normalDirection.z();
		
		
		while (!seed.empty())
		{
			double a, b, c, d;
			size_t ptIndex = seed.front();
			seed.pop_front();
			
			a = ptFeatures[ptIndex].normalDirection.x();
			b = ptFeatures[ptIndex].normalDirection.y();
			c = ptFeatures[ptIndex].normalDirection.z();
			d = ptFeatures[ptIndex].dis;							
			int N;
			N = kdtree.radiusSearch(ptFeatures[ptIndex].pt, searchRadius, pointIdxSearch, pointDistance);
			if (N > 0)
			{
				for (size_t i = 0; i < N; ++i)
				{
					
					if (hasSeg[pointIdxSearch[i]]==false)
					{
						
						float nx1 = ptFeatures[pointIdxSearch[i]].normalDirection.x();
						float ny1 = ptFeatures[pointIdxSearch[i]].normalDirection.y();
						float nz1 = ptFeatures[pointIdxSearch[i]].normalDirection.z();
															   						
						double Cosnormal = ComputeIncludedAngleBetweenVector(nx, ny, nz, nx1, ny1, nz1);
						
						double dis = ComputeDistanceFromPointToPlane(ptFeatures[pointIdxSearch[i]].pt, a, b, c, d);                                         
						    
						    if (Cosnormal > cos(includedAngle) && dis < Pt2PlaneDis)
						{
							unSegment.erase(pointIdxSearch[i]);
							seed.push_back(pointIdxSearch[i]);
							plane.indices.push_back(ptFeatures[pointIdxSearch[i]].ptIndex);
							hasSeg[pointIdxSearch[i]] = true;

						}
					}
				}
			}
			else
			{
				continue;
			}

		}
		
		//cout<<plane.indices.size()<<endl;
		if (plane.indices.size() > minPtNum)
		{
			planes.push_back(plane);
		}
		plane.indices.clear();
		seed.clear();
	}

	std::vector<int>().swap(pointIdxSearch);
	std::vector<float>().swap(pointDistance);
	pointCloudXYZ().swap(inputCloud);
	return planes.size();
}

void planeSegmentation::outputPlanes(const std::string &fileName, pointCloudXYZ &pointCloud, const std::vector <pcl::PointIndices> &planes)
{

	//srand((unsigned)time(NULL));
	//int R, G, B;
	//std::ofstream ofs;
	//ofs.open(fileName.data(),ios_base::out);

	//if (ofs.is_open())
	//{
	//	for (int i = 0; i < planes.size(); i++)
	//	{
	//		R = rand() % 255;
	//		G = rand() % 255;
	//		B = rand() % 255;
	//		for (int j = 0; j < planes[i].indices.size(); j++)
	//		{
	//			ofs << setiosflags(ios::fixed) << setprecision(3) << pointCloud.points[planes[i].indices[j]].x << " "
	//			    << setiosflags(ios::fixed) << setprecision(3) << pointCloud.points[planes[i].indices[j]].y << " "
	//			    << setiosflags(ios::fixed) << setprecision(3) << pointCloud.points[planes[i].indices[j]].z << " "
	//			    << R << " " << G << " " << B << endl;
	//		}
	//	}
	//	ofs.flush();
	//	ofs.close();
	//}
	
	srand((unsigned)time(NULL));
	pointCloudXYZRGB pointCloud_rgb;
	
	int R, G, B;
	for (int i = 0; i < planes.size(); i++)
	{
		R = rand() % 255;
		G = rand() % 255;
		B = rand() % 255;
		for (int j = 0; j < planes[i].indices.size(); j++)
		{
			pcl::PointXYZRGB pt;
			pt.x = pointCloud.points[planes[i].indices[j]].x;
			pt.y = pointCloud.points[planes[i].indices[j]].y;
			pt.z = pointCloud.points[planes[i].indices[j]].z;
			pt.r = R;
			pt.g = G;
			pt.b = B;
			pointCloud_rgb.points.push_back(pt);
		}
	}	
	
	pointCloud_rgb.width=pointCloud_rgb.points.size();
	pointCloud_rgb.height=1;
	pcl::io::savePCDFile(fileName,pointCloud_rgb);
	pointCloudXYZRGB().swap(pointCloud_rgb);
	
}
