#include "plane_seg/VoxelFilter.h"
#include "plane_seg/dataIo.h"

#include <pcl/common/common.h>
#include <Eigen/Dense>
#include<vector>
#include<algorithm>

using namespace  std;

bool cmpVoxelIndex(VoxelFilter::IDPair a, VoxelFilter::IDPair b)
{
	if (a.voxel_idx < b.voxel_idx)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void VoxelFilter::filter(const pointCloudXYZ& pointCloud, pointCloudXYZ &simPointCloud)
{
	//voxel大小的倒数
	float inverse_voxel_size = 1.0f / _voxel_size;

	//获取最大最小
	Eigen::Vector4f min_p, max_p;
	pcl::getMinMax3D(pointCloud, min_p, max_p);

	//计算总共的格子数量
	Eigen::Vector4f gap_p = max_p - min_p;
	unsigned long long max_vx = ceil(gap_p.coeff(0)*inverse_voxel_size) + 1;
	unsigned long long max_vy = ceil(gap_p.coeff(1)*inverse_voxel_size) + 1;
	unsigned long long max_vz = ceil(gap_p.coeff(2)*inverse_voxel_size) + 1;

	//判定格子数量是否超过给定值
	if (max_vx*max_vy*max_vz >= std::numeric_limits<unsigned long long>::max())
	{
		std::cout << "抽稀失败，最大格子数量过多";
	}

	//计算乘子
	unsigned long long mul_vx = max_vy*max_vz;
	unsigned long long mul_vy = max_vz;
	unsigned long long mul_vz = 1;

	//计算所有点的位置
	std::vector<IDPair> id_pairs(pointCloud.points.size());
	unsigned int idx = 0;

	for (size_t i = 0; i < pointCloud.points.size(); ++i)
	{
		//计算编号
		unsigned long long vx = floor((pointCloud.points[i].x - min_p.coeff(0))*inverse_voxel_size);
		unsigned long long vy = floor((pointCloud.points[i].y - min_p.coeff(1))*inverse_voxel_size);
		unsigned long long vz = floor((pointCloud.points[i].z - min_p.coeff(2))*inverse_voxel_size);

		//计算格子编号
		unsigned long long voxel_idx = vx*mul_vx + vy*mul_vy + vz*mul_vz;

		IDPair pair;
		pair.idx = idx;
		pair.voxel_idx = voxel_idx;
		id_pairs.push_back(pair);

		idx++;
	}
        //进行排序
	sort(id_pairs.begin(), id_pairs.end(), cmpVoxelIndex);
	
	//保留每个格子中的一个点
	unsigned int begin_id = 0;
	while (begin_id < id_pairs.size())
	{
		//保留第一个点
		simPointCloud.points.push_back(pointCloud.points[id_pairs[begin_id].idx]);

		//往后相同格子的点都不保留
		unsigned int compare_id = begin_id + 1;
		while (compare_id < id_pairs.size() && id_pairs[begin_id].voxel_idx == id_pairs[compare_id].voxel_idx)
			compare_id++;
		begin_id = compare_id;
	}
}