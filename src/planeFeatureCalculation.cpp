#include "plane_seg/planeFeatureCalculation.h"

#include <set>
#include <list>
#include <deque>
#include <fstream>
#include <vector>

#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <opencv2/opencv.hpp>

using namespace  std;


bool cmpScale(const planeFeaCal::planeFeature &a, const planeFeaCal::planeFeature &b)
{
	if (a.planeScale.area > b.planeScale.area)
	{
		return true;
	}
	return false;
}

void planeFeaCal::calculateTheFeaturesOfPlanes(const pointCloudXYZ &pointCloud,const std::vector <pcl::PointIndices> &planesIndices,
	                                           float mimScale, float angleConstrain, std::vector <planeFeature> &planesFea)
{
	for (size_t i = 0; i < planesIndices.size(); ++i)
	{
		planeFeature planeFea;

		pointCloudXYZ planePointCloud;
		pointCloudXYZ().swap(planePointCloud);

		for (size_t j = 0; j < planesIndices[i].indices.size(); ++j)
		{
			planePointCloud.points.push_back(pointCloud.points[planesIndices[i].indices[j]]);
		}
		calculateEigenFeasByPCA(planePointCloud, planeFea);

		planeBoxBound3d  planeBound;
		calculateTheBoundOfPlane(planePointCloud, planeFea.planeFun, planeBound);
		planeFea.bound = planeBound;

		PlaneSclae planeScale;
		calculateScaleOfPlane(planeBound, planeScale);
		planeFea.planeScale = planeScale;

		Eigen::Vector3f zAxis(0.0f, 0.0f, 1.0f);
		float angle = calculateIncludeAngle(planeFea.planeFun.head(3), zAxis);

		if (planeScale.area > mimScale
			&& (angle<angleConstrain || angle>M_PI - angleConstrain
			|| (angle< M_PI / 2.0f + angleConstrain && angle> M_PI / 2.0f - angleConstrain)))
		{
			planesFea.push_back(planeFea);
		}	
	}

	sort(planesFea.begin(), planesFea.end(), cmpScale);
	cout << planesFea.size() << endl;
	displayPlanesAndBounds(pointCloud, planesFea);
	removeSimilarPlanes(planesFea);
	cout << planesFea.size() << endl;
	displayPlanesAndBounds(pointCloud, planesFea);
}

void planeFeaCal::calculateTheFunctionOfPlane(const pointCloudXYZ &planePointCloud, Eigen::Vector4f &planeFun)
{
	CvMat* pData = cvCreateMat(planePointCloud.points.size(), 3, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(3, 3, CV_32FC1);

	//covariance matrix;
	for (size_t j = 0; j < planePointCloud.points.size(); ++j)
	{
		cvmSet(pData, j, 0, planePointCloud.points[j].x);
		cvmSet(pData, j, 1, planePointCloud.points[j].y);
		cvmSet(pData, j, 2, planePointCloud.points[j].z);
	}

	//principle component analysis to get Eigen vectora and Eigen values;
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eliminate the ambiguity of normal vector;
	//the include angle between the normal and the direction of center point to origion must be acute angle;
	double plCeterx, plCetery, plCeterz;//center point of the plane;
	Eigen::Vector3f normal, plCeter2Origion;//the direction from center point to origion;
	plCeterx = cvmGet(pMean, 0, 0);
	plCetery = cvmGet(pMean, 0, 1);
	plCeterz = cvmGet(pMean, 0, 2);
	plCeter2Origion.x() = -plCeterx;
	plCeter2Origion.y() = -plCetery;
	plCeter2Origion.z() = -plCeterz;

	//the normal 
	normal.x() = cvmGet(pEigVecs, 2, 0);
	normal.y() = cvmGet(pEigVecs, 2, 1);
	normal.z() = cvmGet(pEigVecs, 2, 2);
	float dotProduct;
	dotProduct = normal.dot(plCeter2Origion);
	//if the include angle is obtuse angle, the direction of normal will be flipped;
	float dist;
	dist = sqrt(normal.x()*normal.x() + normal.y()*normal.y() + normal.z()*normal.z());
	if (dotProduct < 0.0f)
	{
		planeFun(0) = -normal.x() / dist;
		planeFun(1) = -normal.y() / dist;
		planeFun(2) = -normal.z() / dist;
	}
	else
	{
		planeFun(0) = normal.x() / dist;
		planeFun(1) = normal.y() / dist;
		planeFun(2) = normal.z() / dist;
	}
	//distance from original to plane;
	double a, b, c;
	a = planeFun(0);
	b = planeFun(1);
	c = planeFun(2);
	planeFun(3) = -(a*plCeterx + b*plCetery + c*plCeterz);

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);
}


void planeFeaCal::calculateEigenFeasByPCA(pointCloudXYZ &planePointCloud, planeFeature &planeFea)
{
	CvMat* pData = cvCreateMat(planePointCloud.points.size(), 3, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 3, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(3, 3, CV_32FC1);

	//covariance matrix;
	for (size_t j = 0; j < planePointCloud.points.size(); ++j)
	{
		cvmSet(pData, j, 0, planePointCloud.points[j].x);
		cvmSet(pData, j, 1, planePointCloud.points[j].y);
		cvmSet(pData, j, 2, planePointCloud.points[j].z);
	}

	//principle component analysis to get Eigen vectora and Eigen values;
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eliminate the ambiguity of normal vector;
	//the include angle between the normal and the direction of center point to origion must be acute angle;
	double plCeterx, plCetery, plCeterz;//center point of the plane;
	Eigen::Vector3f normal, plCeter2Origion;//the direction from center point to origion;
	plCeterx = cvmGet(pMean, 0, 0);
	plCetery = cvmGet(pMean, 0, 1);
	plCeterz = cvmGet(pMean, 0, 2);
	plCeter2Origion.x() = -plCeterx;
	plCeter2Origion.y() = -plCetery;
	plCeter2Origion.z() = -plCeterz;

	//the normal 
	normal.x() = cvmGet(pEigVecs, 2, 0);
	normal.y() = cvmGet(pEigVecs, 2, 1);
	normal.z() = cvmGet(pEigVecs, 2, 2);
	float dotProduct;
	dotProduct = normal.dot(plCeter2Origion);
	//if the include angle is obtuse angle, the direction of normal will be flipped;
	float dist;
	dist = sqrt(normal.x()*normal.x() + normal.y()*normal.y() + normal.z()*normal.z());
	if (dotProduct < 0.0f)
	{
		planeFea.planeFun(0) = -normal.x() / dist;
		planeFea.planeFun(1) = -normal.y() / dist;
		planeFea.planeFun(2) = -normal.z() / dist;
	}
	else
	{
		planeFea.planeFun(0) = normal.x() / dist;
		planeFea.planeFun(1) = normal.y() / dist;
		planeFea.planeFun(2) = normal.z() / dist;
	}
	//distance from original to plane;
	double a, b, c;
	a = planeFea.planeFun(0);
	b = planeFea.planeFun(1);
	c = planeFea.planeFun(2);
	planeFea.planeFun(3) = -(a*plCeterx + b*plCetery + c*plCeterz);

	planeFea.minEigenValue = cvmGet(pEigVals, 0, 2);//the minimum eigen value;
	planeFea.centerPt.x() = plCeterx;
	planeFea.centerPt.y() = plCetery;
	planeFea.centerPt.z() = plCeterz;
	planeFea.ptNum = planePointCloud.points.size();

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);
}

void planeFeaCal::calculateTheBoundOfPlane(const pointCloudXYZ &planePointCloud, const Eigen::Vector4f &planeFun,
	                                       planeBoxBound3d &planeBound)
{
	//计算projectionPointCloud3d的二维投影点;
	pointCloudXY projectionPointCloud2d;
	Projecion3DPlaneTo2D(planeFun, planePointCloud, projectionPointCloud2d);

	//计算PCA坐标系下的4个角点;
	planeBoxBound2d bound2d;
	calculateBoundInPcaCoordinate(projectionPointCloud2d, bound2d);

	//把2d的角点返还回3d;
	Projecion2DPointTo3D(bound2d, planeFun, planeBound);
}

void planeFeaCal::transformPointCloudXaxisToPrincipleDirection(pointCloudXY &projectionPointCloud2d, 
	               Eigen::Vector2f  principleDirection,pointCloudXY &transformPointCloud2d)
{

	float direct_x = principleDirection.x();
	float direct_y = principleDirection.y();


	float x_PCADirect = direct_x;
	float PCADirect_model = sqrt(direct_x*direct_x + direct_y*direct_y);
	//std::cout<<"pca_model: "<<PCADirect_model<<endl;
	float cos_alpha = x_PCADirect / PCADirect_model;
	float alpha = acos(fabs(cos_alpha));

	if (direct_x < 0 && direct_y>0)
		alpha = 3.14159 - alpha;
	if (direct_x < 0 && direct_y < 0)
		alpha = alpha + 3.14159;
	if (direct_x > 0 && direct_y < 0)
		alpha = 6.283185 - alpha;

	for (size_t i = 0; i < projectionPointCloud2d.points.size(); ++i)
	{
		pcl::PointXY pt;
		float x = projectionPointCloud2d.points[i].x;
		float y = projectionPointCloud2d.points[i].y;

		pt.x = x*cos(alpha) + y*sin(alpha);
		pt.y = -x*sin(alpha) + y*cos(alpha);

		transformPointCloud2d.push_back(pt);
	}
}


void planeFeaCal::computePrincipleDirectionOfPointCloud2d(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f & principleDirection)
{
	if (projectionPointCloud2d.points.size() < 5)
		return;

	CvMat* pData = cvCreateMat(projectionPointCloud2d.points.size(), 2, CV_32FC1);
	CvMat* pMean = cvCreateMat(1, 2, CV_32FC1);
	CvMat* pEigVals = cvCreateMat(1, 2, CV_32FC1);
	CvMat* pEigVecs = cvCreateMat(2, 2, CV_32FC1);

	for (size_t i = 0; i < projectionPointCloud2d.points.size(); ++i)
	{
		cvmSet(pData, i, 0, projectionPointCloud2d.points[i].x);
		cvmSet(pData, i, 1, projectionPointCloud2d.points[i].y);
	}
	cvCalcPCA(pData, pMean, pEigVals, pEigVecs, CV_PCA_DATA_AS_ROW);

	//eigne vectors;
	principleDirection.x() = cvmGet(pEigVecs, 0, 0);
	principleDirection.y() = cvmGet(pEigVecs, 0, 1);

	cvReleaseMat(&pEigVecs);
	cvReleaseMat(&pEigVals);
	cvReleaseMat(&pMean);
	cvReleaseMat(&pData);
}

void planeFeaCal::calculateBoundInPcaCoordinate(pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d)
{
	double min_x = transformPointCloud2d.points[0].x;
	double min_y = transformPointCloud2d.points[0].y;

	double max_x = transformPointCloud2d.points[0].x;
	double max_y = transformPointCloud2d.points[0].y;

	for (size_t i = 0; i < transformPointCloud2d.points.size(); ++i)
	{
		//获取边界
		if (min_x > transformPointCloud2d.points[i].x)
			min_x = transformPointCloud2d.points[i].x;
		if (min_y > transformPointCloud2d.points[i].y)
			min_y = transformPointCloud2d.points[i].y;

		if (max_x < transformPointCloud2d.points[i].x)
			max_x = transformPointCloud2d.points[i].x;
		if (max_y < transformPointCloud2d.points[i].y)
			max_y = transformPointCloud2d.points[i].y;
	}

	bound2d.pt[0].x = min_x;
	bound2d.pt[0].y = min_y;

	bound2d.pt[1].x = max_x;
	bound2d.pt[1].y = min_y;

	bound2d.pt[2].x = max_x;
	bound2d.pt[2].y = max_y;

	bound2d.pt[3].x = min_x;
	bound2d.pt[3].y = max_y;
}
void planeFeaCal::Projecion3DPlaneTo2D(const Eigen::Vector4f & planeFun, const pointCloudXYZ &projectionPointCloud3d,
	                                   pointCloudXY &projectionPointCloud2d)
{
	float b, c;
	b = planeFun(1);
	c = planeFun(2);

	if (abs(c) <= 0.1) //认为是与XOY平面垂直  
	{
		if (abs(b) <= 0.1) //往YOZ 投影
		{
			for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
			{
				pcl::PointXY pt;
				pt.x = projectionPointCloud3d.points[i].y;
				pt.y = projectionPointCloud3d.points[i].z;
				projectionPointCloud2d.push_back(pt);
			}
		}
		else //往XOZ 投影 
		{
			for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
			{
				pcl::PointXY pt;
				pt.x = projectionPointCloud3d.points[i].x;
				pt.y = projectionPointCloud3d.points[i].z;
				projectionPointCloud2d.push_back(pt);
			}
		}
	}
	else  //不与XOY垂直  则往XOY投影
	{
		for (size_t i = 0; i < projectionPointCloud3d.size(); ++i)
		{
			pcl::PointXY pt;
			pt.x = projectionPointCloud3d.points[i].x;
			pt.y = projectionPointCloud3d.points[i].y;
			projectionPointCloud2d.push_back(pt);
		}
	}
}


void planeFeaCal::calculate2dBound(const pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d)
{
	double min_x = transformPointCloud2d.points[0].x;
	double min_y = transformPointCloud2d.points[0].y;

	double max_x = transformPointCloud2d.points[0].x;
	double max_y = transformPointCloud2d.points[0].y;

	for (size_t i = 0; i<transformPointCloud2d.points.size(); ++i)
	{
		//获取边界
		if (min_x>transformPointCloud2d.points[i].x)
			min_x = transformPointCloud2d.points[i].x;
		if (min_y > transformPointCloud2d.points[i].y)
			min_y = transformPointCloud2d.points[i].y;

		if (max_x < transformPointCloud2d.points[i].x)
			max_x = transformPointCloud2d.points[i].x;
		if (max_y < transformPointCloud2d.points[i].y)
			max_y = transformPointCloud2d.points[i].y;
	}

	bound2d.pt[0].x = min_x;
	bound2d.pt[0].y = min_y;

	bound2d.pt[1].x = max_x;
	bound2d.pt[1].y = min_y;

	bound2d.pt[2].x = max_x;
	bound2d.pt[2].y = max_y;

	bound2d.pt[3].x = min_x;
	bound2d.pt[3].y = max_y;
}

void planeFeaCal::InverseTransformToOriginalCoordinate(const planeBoxBound2d bound2dInPCA, Eigen::Vector2f  principleDirection,
	                                                    planeBoxBound2d &bound2dInOri)
{

	float direct_x = principleDirection.x();
	float direct_y = principleDirection.y();


	float x_PCADirect = direct_x;
	float PCADirect_model = sqrt(direct_x*direct_x + direct_y*direct_y);
	//std::cout<<"pca_model: "<<PCADirect_model<<endl;
	float cos_alpha = x_PCADirect / PCADirect_model;
	float alpha = acos(fabs(cos_alpha));

	if (direct_x < 0 && direct_y>0)
		alpha = 3.14159 - alpha;
	if (direct_x < 0 && direct_y < 0)
		alpha = alpha + 3.14159;
	if (direct_x > 0 && direct_y < 0)
		alpha = 6.283185 - alpha;


	for (int i = 0; i < 4; i++)
	{
		float x = bound2dInPCA.pt[i].x;
		float y = bound2dInPCA.pt[i].y;

		bound2dInOri.pt[i].x = x*cos(alpha) - y*sin(alpha);
		bound2dInOri.pt[i].y = x*sin(alpha) + y*cos(alpha);
	}
}

void planeFeaCal::Projecion2DPointTo3D(const planeBoxBound2d bound2d, const Eigen::Vector4f &planeFun, planeBoxBound3d &bound3d)
{

	float a, b, c, d;
	a = planeFun(0);
	b = planeFun(1);
	c = planeFun(2);
	d = planeFun(3);
	for (int i = 0; i < 4; i++)
	{
		if (abs(c) <= 0.1) //认为是与XOY平面垂直  
		{
			if (abs(b) <= 0.1) //往YOZ 投影
			{
				float x, y, z;
				y = bound2d.pt[i].x;
				z = bound2d.pt[i].y;

				if (a != 0)
				{
					x = -(b*y + c*z + d) / a;
				}
				else
				{
					x = 0.0f;
				}
				bound3d.pt[i].x = x;
				bound3d.pt[i].y = y;
				bound3d.pt[i].z = z;
			}

			else //往XOZ 投影 
			{
				float x, y, z;
				x = bound2d.pt[i].x;
				z = bound2d.pt[i].y;

				if (b != 0)
				{
					y = -(a*x + c*z + d) / b;
				}
				else
				{
					y = 0.0f;
				}
				bound3d.pt[i].x = x;
				bound3d.pt[i].y = y;
				bound3d.pt[i].z = z;

			}
		}
		else  //不与XOY垂直  则往XOY投影
		{
			float x, y, z;
			x = bound2d.pt[i].x;
			y = bound2d.pt[i].y;

			if (c != 0)
			{
				z = -(a*x + b*y + d) / c;
			}
			else
			{
				z = 0.0f;
			}
			bound3d.pt[i].x = x;
			bound3d.pt[i].y = y;
			bound3d.pt[i].z = z;
		}
	}
}

void planeFeaCal::calculateScaleOfPlane(const planeBoxBound3d &bound3d, PlaneSclae &planeScale)
{
	float width, length;
	float dertax, dertay, dertaz;

	dertax = bound3d.pt[0].x - bound3d.pt[1].x;
	dertay = bound3d.pt[0].y - bound3d.pt[1].y;
	dertaz = bound3d.pt[0].z - bound3d.pt[1].z;
	width = sqrt(dertax*dertax + dertay*dertay + dertaz*dertaz);

	dertax = bound3d.pt[2].x - bound3d.pt[1].x;
	dertay = bound3d.pt[2].y - bound3d.pt[1].y;
	dertaz = bound3d.pt[2].z - bound3d.pt[1].z;
	length = sqrt(dertax*dertax + dertay*dertay + dertaz*dertaz);

	float area;
	area = length*width;
	planeScale.area = area;
	planeScale.width = width;
	planeScale.length = length;
}

float planeFeaCal::calculateIncludeAngle(const Eigen::Vector3f & f1, const Eigen::Vector3f & f2)
{
	float dotProduct, includeAngle;
	dotProduct = f1.dot(f2);

	if (dotProduct > 1.0)
	{
		dotProduct = 1.0;
	}

	if (dotProduct < -1.0)
	{
		dotProduct = -1.0;
	}

	includeAngle = acos(dotProduct);

	return includeAngle;
}


void planeFeaCal::removeSimilarPlanes(std::vector<planeFeature> &planesFea)
{
	if (planesFea.size() < 2)
	{
		return;
	}

	sort(planesFea.begin(), planesFea.end(), cmpScale);
	vector<planeFeature> UniquePlanes;
	for (size_t i = 0; i < planesFea.size() - 1; ++i)
	{
		bool isUnique = true;
		PlaneRelation relation;
		for (size_t j = i + 1; j < planesFea.size(); ++j)
		{
			pair< float, float > dn;
			distAndNormalDevBetweenPlanes(planesFea[i], planesFea[j], dn);
			if (dn.first < 0.2f && dn.second < 5.0f*M_PI / 180.0)
			{
				relation = calculatePlaneRelation(planesFea[i], planesFea[j]);
				if (relation==INCLUSIVE||relation==INTERSECTION)
				{
					isUnique = false;
					break;
				}	
			}
		}
		if (isUnique)
		{
			UniquePlanes.push_back(planesFea[i]);
		}
	}

	UniquePlanes.push_back(planesFea[planesFea.size() - 1]);
	vector<planeFeature>().swap(planesFea);
	planesFea = UniquePlanes;
}

planeFeaCal::PlaneRelation planeFeaCal::calculatePlaneRelation(const planeFeature &pl1, const planeFeature &pl2)
{
	planeBoxBound2d pl1_bound2d, pl2_bound2d;
	Projecion3DBoundTo2D(pl1.bound, pl1.planeFun, pl1_bound2d);
	Projecion3DBoundTo2D(pl2.bound, pl2.planeFun, pl2_bound2d);

	bool has_pt_in = false;
	bool has_pt_off = false;

	for (int i = 0; i < 4; ++i)
	{
		if (PtInPolygon(pl2_bound2d.pt[i], pl1_bound2d))
		{
			has_pt_in = true;
		}
		else
		{
			has_pt_off = true;
		}
	}

	if (has_pt_in  && has_pt_off)
	{
		return INTERSECTION;
	}
	if (has_pt_in && !has_pt_off)
	{
		return INCLUSIVE;
	}

	return DISJOINT;
}

int  planeFeaCal::Judge_direction(double x1, double y1, double x2, double y2, double testx, double testy)
{
	double  direction;

	direction = x1*y2 - x1*testy +
		x2*testy - x2*y1 +
		testx*y1 - testx*y2;

	if (fabs(direction)<0.0000001)
		return(0);
	else if (direction>0)
		return(1);//left
	else
		return(-1);//right
}

bool planeFeaCal::PtInPolygon(const pcl::PointXY &p, const planeBoxBound2d &bound2d)
{
	// 交点个数
	int nCross = 0;
	double x1, y1, x2, y2;
	x1 = p.x;
	y1 = p.y;
	x2 = 1e10;
	y2 = 1e10;
	pcl::PointXY p1, p2;
	int direction1, direction2;
	for (int i = 0; i < 4; i++)
	{
		p1 = bound2d.pt[i];
		p2 = bound2d.pt[(i + 1) % 4];// 最后一个点与第一个点连线
		direction1 = Judge_direction(p1.x, p1.y, p2.x, p2.y, x1, y1);
		direction2 = Judge_direction(p1.x, p1.y, p2.x, p2.y, x2, y2);
		if (direction1*direction2 < 0)
		{
			direction1 = Judge_direction(x1, y1, x2, y2, p1.x, p1.y);
			direction2 = Judge_direction(x1, y1, x2, y2, p2.x, p2.y);
			if (direction1*direction2 < 0)
				nCross += 1;
		}
	}
	// 交点为偶数，点在多边形之外
	return (nCross % 2 == 1);
}

void planeFeaCal::Projecion3DBoundTo2D(const planeBoxBound3d bound3d, const Eigen::Vector4f &planeFun, planeBoxBound2d &bound2d)
{
	float b, c;
	b = planeFun[1];
	c = planeFun[2];

	if (abs(c) <= 0.1) //认为是与XOY平面垂直  
	{
		if (abs(b) <= 0.1) //往YOZ 投影
		{
			for (size_t i = 0; i < 4; ++i)
			{
				pcl::PointXY pt;
				bound2d.pt[i].x = bound3d.pt[i].y;
				bound2d.pt[i].y = bound3d.pt[i].z;
			}
		}
		else //往XOZ 投影 
		{
			for (size_t i = 0; i < 4; ++i)
			{
				pcl::PointXY pt;
				bound2d.pt[i].x = bound3d.pt[i].x;
				bound2d.pt[i].y = bound3d.pt[i].z;
			}
		}
	}
	else  //不与XOY垂直  则往XOY投影
	{
		for (size_t i = 0; i < 4; ++i)
		{
			pcl::PointXY pt;
			bound2d.pt[i].x = bound3d.pt[i].x;
			bound2d.pt[i].y = bound3d.pt[i].y;
		}
	}
}

void planeFeaCal::displayPlaneAndBound(const pointCloudXYZ &pointCloud, const planeFeature &planeFea)
{
	srand((unsigned)time(NULL));
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB(new pcl::PointCloud<pcl::PointXYZRGB>);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("planes"));
	viewer->setBackgroundColor(255, 255, 255);

	int R, G, B;
	R = rand() % 255;
	G = rand() % 255;
	B = rand() % 255;

	for (size_t i = 0; i < pointCloud.size(); ++i)
	{
		pcl::PointXYZRGB TempPoint;
		TempPoint.x = pointCloud.points[i].x;
		TempPoint.y = pointCloud.points[i].y;
		TempPoint.z = pointCloud.points[i].z;
		TempPoint.r = R;
		TempPoint.g = G;
		TempPoint.b = B;
		cloudRGB->points.push_back(TempPoint);
	}
	viewer->addPointCloud(cloudRGB);


	int n = 0;
	char t[256];
	string s;


	pcl::PointXYZ pt1, pt2;
	for (int j = 0; j < 3; j++)
	{
		pt1.x = planeFea.bound.pt[j].x;
		pt1.y = planeFea.bound.pt[j].y;
		pt1.z = planeFea.bound.pt[j].z;

		pt2.x = planeFea.bound.pt[j + 1].x;
		pt2.y = planeFea.bound.pt[j + 1].y;
		pt2.z = planeFea.bound.pt[j + 1].z;

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
		viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	}

	pt1.x = planeFea.bound.pt[3].x;
	pt1.y = planeFea.bound.pt[3].y;
	pt1.z = planeFea.bound.pt[3].z;

	pt2.x = planeFea.bound.pt[0].x;
	pt2.y = planeFea.bound.pt[0].y;
	pt2.z = planeFea.bound.pt[0].z;

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
	viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

	sprintf(t, "%d", n);
	n++;
	s = t;
	viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

	while (!viewer->wasStopped())
	{
		viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(1000));
	}
}

void planeFeaCal::displayPlanesAndBounds(const pointCloudXYZ &pointCloud, const std::vector<planeFeature>  &planesFea)
{
	srand((unsigned)time(NULL));
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloudRGB(new pcl::PointCloud<pcl::PointXYZRGB>);

	boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer(new pcl::visualization::PCLVisualizer("planes"));
	viewer->setBackgroundColor(255, 255, 255);

	int R, G, B;
	R = rand() % 255;
	G = rand() % 255;
	B = rand() % 255;

	for (size_t i = 0; i < pointCloud.size(); ++i)
	{
		pcl::PointXYZRGB TempPoint;
		TempPoint.x = pointCloud.points[i].x;
		TempPoint.y = pointCloud.points[i].y;
		TempPoint.z = pointCloud.points[i].z;
		TempPoint.r = R;
		TempPoint.g = G;
		TempPoint.b = B;
		cloudRGB->points.push_back(TempPoint);
	}
	viewer->addPointCloud(cloudRGB);


	int n = 0;
	char t[256];
	string s;


	pcl::PointXYZ pt1, pt2;

	for (size_t i = 0; i < planesFea.size(); ++i)
	{
		for (int j = 0; j < 3; j++)
		{
			pt1.x = planesFea[i].bound.pt[j].x;
			pt1.y = planesFea[i].bound.pt[j].y;
			pt1.z = planesFea[i].bound.pt[j].z;

			pt2.x = planesFea[i].bound.pt[j + 1].x;
			pt2.y = planesFea[i].bound.pt[j + 1].y;
			pt2.z = planesFea[i].bound.pt[j + 1].z;

			sprintf(t, "%d", n);
			n++;
			s = t;
			viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
			viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

			sprintf(t, "%d", n);
			n++;
			s = t;
			viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);

		}

		pt1.x = planesFea[i].bound.pt[3].x;
		pt1.y = planesFea[i].bound.pt[3].y;
		pt1.z = planesFea[i].bound.pt[3].z;

		pt2.x = planesFea[i].bound.pt[0].x;
		pt2.y = planesFea[i].bound.pt[0].y;
		pt2.z = planesFea[i].bound.pt[0].z;

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addLine(pt1, pt2, 1.0, 0.0, 0.0, s);
		viewer->setShapeRenderingProperties(pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 2, s);

		sprintf(t, "%d", n);
		n++;
		s = t;
		viewer->addSphere(pt1, 0.5, 1.0, 0.0, 0.0, s);
	}


	while (!viewer->wasStopped())
	{
		viewer->spinOnce();
		boost::this_thread::sleep(boost::posix_time::microseconds(1000));
	}
}


void planeFeaCal::outputPlanesFea(const string &filename, const vector<planeFeaCal::planeFeature> &planesFea)
{
        ofstream ofs;
	ofs.open(filename.data(),ios_base::out);
	ofs << planesFea.size() << endl;
	for (size_t i = 0; i < planesFea.size(); ++i)
	{
		ofs << setiosflags(ios::fixed) << setprecision(3) << planesFea[i].planeFun(0) << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFea[i].planeFun(1) << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFea[i].planeFun(2) << "  "
			<< setiosflags(ios::fixed) << setprecision(3) << planesFea[i].planeFun(3) << "  ";


		for (int j = 0; j < 4; ++j)
		{
			ofs << setiosflags(ios::fixed) << setprecision(3) << planesFea[i].bound.pt[j].x << "  "
				<< setiosflags(ios::fixed) << setprecision(3) << planesFea[i].bound.pt[j].y << "  "
				<< setiosflags(ios::fixed) << setprecision(3) << planesFea[i].bound.pt[j].z << "  ";
		}
		ofs << endl;
	}
	ofs.close();
}
