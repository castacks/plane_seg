#include "plane_seg/dataIo.h"

#include <iostream>
#include <fstream>

#include <pcl/io/pcd_io.h>
#include <pcl/io/ply_io.h>


using namespace  std;


bool dataIo::readPointCloudFromPcdFileA(const std::string &fileName, pointCloudXYZ &pointCloud)
{
	if (pcl::io::loadPCDFile<pcl::PointXYZ>(fileName, pointCloud) == -1) //* load the file
	{
		PCL_ERROR("cloud read pcd file\n");
		
		return false;
	}

	return true;
}

bool dataIo::readPointCloudFromPlyFileA(const std::string &fileName, pointCloudXYZ &pointCloud)
{
	if (pcl::io::loadPLYFile<pcl::PointXYZ>(fileName, pointCloud) == -1) //* load the file
	{
		PCL_ERROR("cloud read ply file\n");
		return false;
	}

	return true;
}
