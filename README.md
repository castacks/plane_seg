# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

**plane_seg** is a package for  planes extraction and plane features calculations.

+parameters for plane segmentation:

+   "input_pcd"                the filename of the pcd file;

+   "downsample_resolution"    the resolution for downsample

+   "radiusFea"                the radius of the neiborhood for normal calculation

+   "includedAngle"            the include angle threshold for normals belonging to  one plane

+   "Pt2PlaneDis"              the distance threshold for a piont to a plane

+   "searchRadius"             the radius for neiboring point search 

+   "mimPtNum"                 the minmum point number for a plane

+   "plane_seg_pcd"            the output filename (*.pcd) for plane segmentation results

+   "plane_fea_txt"            the output filename(*) for plane features


+format of the  plane_fea_txt file 

    +the first line is: the number of planes
    
    +each of the following lines is: a b c d (plane funtion: ax+by+cz+d=0) pt1.x pt1.y pt1.z pt2.x pt2.y pt2.z pt3.x pt3.y pt3.z pt4.x pt4.y pt4.z (the four points of the Minimum bounding rectangle)


### How do I get set up? ###

# Create workspace catkin_ws, fetch repositories and build: #

source /opt/ros/indigo/setup.bash # init environment

mkdir -p catkin_ws/src

cd catkin_ws/src

catkin_init_workspace


git clone git clone git@bitbucket.org:castacks/plane_seg.git

cd ..

catkin_make -DCMAKE_BUILD_TYPE=Release

source devel/setup.bash

roslaunch plane_seg planeseg_node

### Who do I talk to? ###

dongzhenwhu@whu.edu.cn

### License ###
[This software is BSD licensed.](http://opensource.org/licenses/BSD-3-Clause)

Copyright (c) 2015, Carnegie Mellon University
All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.