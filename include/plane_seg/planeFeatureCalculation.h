#ifndef PLANEFEATURECALCULATION_H
#define PLANEFEATURECALCULATION_H

#include "dataIo.h"

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/PointIndices.h>

#include <Eigen/Dense>

class planeFeaCal
{
public:
	enum PlaneRelation
	{
		DISJOINT=1,
		INTERSECTION,
		INCLUSIVE
	};

	struct planeBoxBound3d
	{
		pcl::PointXYZ pt[4];
		planeBoxBound3d()
		{
			for (int i = 0; i < 4; i++)
			{
				pt[i].x = pt[i].y = pt[i].z = 0.0f;
			}
		}
	};

	struct planeBoxBound2d
	{
		pcl::PointXY pt[4];
		planeBoxBound2d()
		{
			for (int i = 0; i < 4; i++)
			{
				pt[i].x = pt[i].y = 0.0f;
			}
		}
	};

	struct PlaneSclae
	{
		float area;
		float width;
		float length;
		PlaneSclae()
		{
			area = width = length = 0.0f;
		}
	};

	struct planeFeature
	{
		Eigen::Vector4f planeFun;
		Eigen::Vector3f centerPt;
		planeBoxBound3d bound;
		float minEigenValue;
		PlaneSclae planeScale;
		size_t ptNum;
		planeFeature()
		{
			minEigenValue = 0.0f;
			ptNum = 0;
		}
	};

	/*plane features calculation*/
	void calculateTheFeaturesOfPlanes(const pointCloudXYZ &pointCloud, const std::vector <pcl::PointIndices> &planesIndices,
		                          float mimScale, float angleConstrain,std::vector <planeFeature> &planesFea);

	void calculateEigenFeasByPCA(pointCloudXYZ &planePointCloud, planeFeature &planeFea);

	void calculateTheFunctionOfPlane(const pointCloudXYZ &planePointCloud, Eigen::Vector4f &planeFun);



	void calculateTheBoundOfPlane(const pointCloudXYZ &planePointCloud, const Eigen::Vector4f &planeFun,
		planeBoxBound3d &planeBound);

	void calculateProjectionPointCloud(const pointCloudXYZ &planePointCloud, const Eigen::Vector4f &planeFun, pointCloudXYZ &projectionPointCloud);

	void Projecion3DPlaneTo2D(const Eigen::Vector4f &planeFun, const pointCloudXYZ &projectionPointCloud3d,
		pointCloudXY &projectionPointCloud2d);
	void computePrincipleDirectionOfPointCloud2d(pointCloudXY &projectionPointCloud2d, Eigen::Vector2f & principleDirection);
	void transformPointCloudXaxisToPrincipleDirection(pointCloudXY &projectionPointCloud2d, 
		                                              Eigen::Vector2f  principleDirection,pointCloudXY &transformPointCloud2d);
	void calculateBoundInPcaCoordinate(pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d);

	void calculate2dBound(const pointCloudXY &transformPointCloud2d, planeBoxBound2d &bound2d);
	void InverseTransformToOriginalCoordinate(const planeBoxBound2d bound2dInPCA, Eigen::Vector2f  principleDirection, planeBoxBound2d &bound2dInOri);

	void Projecion2DPointTo3D(const planeBoxBound2d bound2d, const Eigen::Vector4f &planeFun, planeBoxBound3d &bound3d);

	void calculateScaleOfPlane(const planeBoxBound3d &bound3d, PlaneSclae &planeScale);
	float calculateIncludeAngle(const Eigen::Vector3f & f1, const Eigen::Vector3f & f2);

	void removeSimilarPlanes(std::vector<planeFeature> &planesFea);

	PlaneRelation calculatePlaneRelation(const planeFeature &pl1, const planeFeature &pl2);
	void Projecion3DBoundTo2D(const planeBoxBound3d bound3d, const Eigen::Vector4f &planeFun, planeBoxBound2d &bound2d);
	int  Judge_direction(double x1, double y1, double x2, double y2, double testx, double testy);
	bool PtInPolygon(const pcl::PointXY &p, const planeBoxBound2d &bound2d);

	void displayPlaneAndBound(const pointCloudXYZ &pointCloud, const planeFeature  &planeFea);
	void displayPlanesAndBounds(const pointCloudXYZ &pointCloud, const std::vector<planeFeature>  &planesFea);

    void outputPlanesFea(const std::string &filename, const std::vector<planeFeature> &planesFea);


	void distAndNormalDevBetweenPlanes(const planeFeature &plane1, const planeFeature &plane2, std::pair< float, float > &dn)
	{
		dn.first = calculatePointToPlaneDist(plane1.centerPt, plane2);
		dn.second = normalDeviationAcuteAngle(plane1.planeFun.head(3), plane2.planeFun.head(3));
	}

	float calculatePointToPlaneDist(const Eigen::Vector3f &pt, const planeFeature &planePara)
	{
		return fabs(pt.dot(planePara.planeFun.head(3)) + planePara.planeFun[3]);
	}

	float normalDeviationAcuteAngle(const Eigen::Vector3f &normal1, const Eigen::Vector3f &normal2)
	{
		float dotProduct, acuteAngle;
		dotProduct = fabs(normal1.dot(normal2));
		if (dotProduct > 1.0)
		{
			dotProduct = 1.0;
		}
		acuteAngle = acos(dotProduct);
		return acuteAngle;
	}

};
#endif
