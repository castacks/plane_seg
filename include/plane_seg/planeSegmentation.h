#ifndef PLANESEGMENTATION
#define PLANESEGMENTATION

#include "dataIo.h"
#include "featureCalculation.h"

#include <pcl/PointIndices.h>
#include <math.h>


#include <iostream>
#include <fstream>
#include <iomanip>

class planeSegmentation: public featureCalculation
{
public:

	

  //plane segmentation;
  size_t planeSegmentationBasedOnRegionGrowingRadius(pointCloudXYZ &pointCloud, 
                                                     std::vector<featureCalculation::eigenFeature> &ptFeatures,
	                                             float includedAngle, float Pt2PlaneDis, float searchRadius,int minPtNum,
				                      std::vector <pcl::PointIndices> &planes);
	

  void outputPlanes(const std::string &fileName, pointCloudXYZ &pointCloud, const std::vector <pcl::PointIndices> &planes);

protected:


private:
	double ComputeIncludedAngleBetweenVector(float vx1, float vy1, float vz1, float vx2, float vy2, float vz2)
	{
		double n_n1, n_n,n1_n1,Cosnormal;
                n_n1  = vx1*vx2 + vy1*vy2 + vz1*vz2;
		n_n = sqrt(vx1*vx1 + vy1*vy1 + vz1*vz1);
		n1_n1 = sqrt(vx2*vx2 + vy2*vy2 + vz2*vz2);
		Cosnormal = fabs(n_n1 / (n_n*n1_n1));
		return Cosnormal;
	}

	
	double ComputeDistanceFromPointToPlane(pcl::PointXYZ & a_point, float a, float b, float c, float d)
	{
		
		float x1, y1, z1;
		
		x1 = a_point.x; y1 = a_point.y; z1 = a_point.z;
		double dis;
		double g = sqrt(a*a + b*b + c*c);
		double f1 = a*x1;
		double f2 = b*y1;
		double f3 = c*z1;
		double f4 = d;
		double f = abs(f1 + f2 + f3 + f4);
		dis = (f / g);
		return dis;
	}

};

#endif
