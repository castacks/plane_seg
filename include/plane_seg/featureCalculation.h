
#ifndef FEATURECALCULATION
#define FEATURECALCULATION


#include "dataIo.h"
#include <vector>
#include <Eigen/src/Core/Matrix.h>

class featureCalculation:public dataIo
{
public:

	struct eigenFeature
	{
		Eigen::Vector3f normalDirection;
		Eigen::Vector3f principleDirection;
		unsigned short dimension;
		float lamada1;
		float lamada2;
		float lamada3;
		double curvature;
		double dis;//原点到平面的距离;
		pcl::PointXYZ pt;
		size_t ptIndex;
	};

	bool calculateEigenFeaturesBasedOnNearestKSearch(pointCloudXYZ &cloud, unsigned short ptNum, std::vector<eigenFeature> &ptFeatures);
	bool calculateEigenFeaturesBasedOnRadiusSearch(pointCloudXYZ &cloud, float radius, std::vector<eigenFeature> &ptFeatures);

	bool calculateEigenFeature(pointCloudXYZ &cloud, std::vector<int> &searchIndices, eigenFeature &ptFeature);
        
        void outputFeature(const std::string fileName, const std::vector<eigenFeature> &ptFeatures);
	
protected:
private:
};
#endif
