#pragma once

#include <vector>
#include <limits>
#include <iostream>

#include "dataIo.h"

class VoxelFilter
{
public:
	float _voxel_size;

	VoxelFilter(float voxel_size):_voxel_size(voxel_size) {}

	struct IDPair
	{
		IDPair() :idx(0), voxel_idx(0) {}

		unsigned long long voxel_idx;
		unsigned int idx;
	};
	void filter(const pointCloudXYZ& pointCloud, pointCloudXYZ &simPointCloud);
	
};
